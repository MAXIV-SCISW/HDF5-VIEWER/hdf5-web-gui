# HDF5 WEB GUI

1. [Quick Restart Instructions For MAX IV](#quick-restart-instructions)
2. [Overview](#overview)
3. [Screenshots](#screenshots)
4. [Installation](#installation)
    1. [BUILD & RUN - DEMO MODE](#build-run-demo-mode)
    2. [BUILD & RUN - CAS & HTTPS MODE](#build-run-cas-https-mode)
    3. [RUN DEVELOPMENT MODE](#run-development-mode)
5. [Demo Server](#demo-server)


## MAX IV

### QUICK RESTART INSTRUCTIONS

In case the VM on which this has been installed was rebooted, the
back-end server may need to be restarted, and the front-end may need to be
restarted too.  This will need to be done with the service account "gitlab."

1. Restart back-end:
```bash
ssh w-v-hdf5view-0
  sudo su - gitlab
  cd /var/www/hdf5-viewer/hdf5-simple-server/
  make stop
  make start-cas-https-maxiv
```

It could happen that an error occurs at this point concerning "cgroup
mountpoints."  This has to do with a docker error and can be fixed with:
```bash
  make cgroup-error-fix
```

2. Restart front-end, still as the "gitlab" user:
```bash
  cd /var/www/hdf5-viewer/hdf5-web-gui/
  make stop
  make start-cas-https-maxiv
```

For both parts of the application, one can watch the logs:
```bash
  make logs
```

And check that the application works:
  [HDF5 Viewer](https://hdf5view.maxiv.lu.se/)

### CI
The continuous integration (CI) pipeline is only setup to work inside of the
MAX IV network at present.

When changes to particular files on the master branch are made and pushed to
the repository, the CI pipeline will be triggered, which will result in:
1. A new docker image being built and pushed to two registries:
    1. An internal MAX IV registry
    2. A public [gitlab.com registry](https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-web-gui/container_registry)
2. The new image being pulled to two servers:
    1. The internal, production HDF5 Viewer server
    2. The publicly available, [Demo HDF5 View Server](http://demo.maxiv.lu.se/)
3. The docker container will be restarted on both servers

For more, see the CI configuration: [.gitlab-ci.yml](https://gitlab.maxiv.lu.se/scisw/hdf5-viewer/hdf5-web-gui/-/blob/master/.gitlab-ci.yml)


## OVERVIEW

This is a web GUI for viewing HDF5 files.

The HDF Group [made their own web ui](http://data.hdfgroup.org/),
which was the inspiration for this project, but that project seemed to be more
of a proof of concept, and I wanted to use different tools for the interface
than what they had chosen, namely to make something responsive and to use a
different plotting library.

This project makes use of
[HDF5 Simple Server](https://gitlab.com/MAXIV-HDF5/hdf5-web-gui) for the
backend - kind of like a stripped down version of h5serv from the HDF5 Group.

This web GUI is written in javascript, sprinkled with a bit of jquery, and
makes use of the [plotly](https://plot.ly/javascript/) graphing libraries.

### SCREENSHOTS

The HDF5 Web GUI, browsing data, viewing an image in a laptop web browser:

![3D surface plot](screenshots/screenshot-entire-gui-2.png)

3-dimensional, rotatable, zoomable plots of data can be made. Here's a
screenshot:

![3D surface plot](screenshots/screenshot-3d-plot.png)

Here's a screenshot of a 2-dimensional zoomable contour plot from a series of
images, where hot pixels have been automatically supressed, and with x and y
profile histograms that match the displayed image:

![2D density plot](screenshots/screenshot-masked-downsampled.png)

And here is a view the application when displayed on a mobile device, the hidden
menu has been clicked, which can then be hidden to just display the plot:

![Mobile view](screenshots/screenshot-mobile-view.png)

## INSTALLATION

Installing this web application can be done in two different ways:
1. Using the supplied docker configuration files
2. On a server which already has an existing web server running

When using either method, this HDF5 Viewer can be on the same machine where
hdf5-simple-server is installed, or they can be on seperate machines.


## INSTALLATION WITH DOCKER

### GIT THE CODE

First thing to do is download the code.

Git code from the private MAX IV repository:
```bash
git clone git@gitlab.maxiv.lu.se:scisw/hdf5-viewer/hdf5-web-gui.git
```

Or from the public gitlab repository:
```bash
git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-web-gui.git
```


### BUILD & RUN - DEMO MODE

The easiest thing to do first is to run in the so-called demo mode, which here
means that the application is run with http (therefore no ssl certificates
needed) and no authentication is needed - everything is run as the root user
within a docker container.

The only dependencies in this case are docker, docker-compose, and make.

There is a Makefile present which is intended to simplify the building and
running of the docker images. For usage information:
```bash
make
```

Assuming this web gui repository is to be run as a front-end to the
hdf5-simple-server, it can be nice to start them and install them at the same
time, and monitor their logs and stop and start them as needed.

So, using two separate terminals:

#### Terminal for hdf5-simple-server
```bash
git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-simple-server.git
cd hdf5-simple-server/
make build
make start-demo
make logs
```
At the end of the image build, which should take around 10 minutes, a message
should appear:

*Successfully tagged maxiv-scisw-hdf5/hdf5-simple-server:local*

Logs should display some output.

#### Check Server Output
Now check the server output in a web browser, using the localhost:

[http://localhost:8081](http://localhost:8081)


It should look like the screenshot below:

![screenshot folder](screenshots/screenshot_example_output_demo_folder.png)


#### Terminal for hdf5-web-gui
```bash
git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-web-gui.git
cd hdf5-web-gui/
make build
make start-demo
make logs
```
At the end of the image build, which should take around 1 minute, a message
should appear:

*Successfully tagged maxiv-scisw-hdf5/hdf5-web-gui:local*

Logs will start to appear when url accessed.


#### Check GUI Output
Then hopefully the front-end will also be working, and is successfully
communicating with the server, both now running on the localhost:

[http://localhost:8082/](http://localhost:8082/)

![screenshot](screenshots/screenshot_web_gui.png)


#### Stopping the applications:
```bash
make stop
```

And that's it for running in demo mode!


### BUILD & RUN - CAS & HTTPS MODE
Running with CAS & https is essentially the same as running demo-mode with
the added complications of:
1. Having valid ssl certificates
2. Having a CAS authentication server already setup

#### BUILD
This step is just like the build step when running in any other mode:
```bash
git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-web-gui.git
cd hdf5-web-gui/
make build
```
At the end of the image build, which should take around 1 minute, a message
should appear:

*Successfully tagged maxiv-scisw-hdf5/hdf5-web-gui:local*


#### CONFIGURE FOR CAS & HTTPS MODE
Before running, a few configuration files need to be altered. I use vim in
the following instructions, but any text editor will do.

The applicaiton is run in a docker container, orchestraed by a docker-compose
configuration file. The hostname, ports used, and location of ssl certificates
will need to be set to their proper values, in the following lines:
```bash
vim docker/docker-compose-cas-https.yml

    - /etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.crt:/app/certs/ssl_cert.crt:ro
    - /etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.key:/app/certs/ssl_cert.key:ro

    hostname: jasbru.maxiv.lu.se

    ports:
      - "443:8443"
      - "80:8080"
```

There are two spots where "server_name" is set in the nginx config file that
will need to be changed, and should match the hostname used in
docker-compose-cas-https.yml:
```bash
vim docker/nginx-cas-https.conf
    server_name jasbru.maxiv.lu.se;
```

Set the address of the back-end server, it will need to be a url that begins
with https:
```bash
vim config/config-cas-https.json
    "hdf5DataServer": "https://jasbru.maxiv.lu.se:8081",
```


#### RUN IN CAS & HTTPS MODE
Start the application and watch the logs:
```bash
make start-cas-https
make logs
```

#### CHECK GUI OUTPUT IN CAS & HTTPS MODE
Now hopefully the front-end will be working, and when accessed for the
first time, for example:

[https://jasbru.maxiv.lu.se:8443/](https://jasbru.maxiv.lu.se:8443/)

the page will automatically be redirected to the CAS authentication server, the
address of which is set in a
[server configuration file](https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-simple-server/-/blob/master/config/config-cas-https.cfg).

After successfully logging in, a welcome message should appear along with a
list of the data directory contents on the left hand side:

![screenshot welcome](screenshots/screenshot_welcome.png)


### RUN DEVELOPMENT MODE
This method of running the code is meant to be done without docker, which
would then require a separate web server to be running on the same machine,
such as apache or nginx.

#### RUN DEVELOPMENT MODE - DEMO
In demo-mode (without any https and no logging-in) you just need a working
web server running on some port and using http.

Edit the configuration file to point to the location of the server, which
should also be running in demo mode and using localhost. Note the use of http
and not https. Also, set the location of this front-end repository relative to
the base directory of the web server:
```bash
vim config/config-dev-demo.json
    "hdf5DataServer": "http://localhost:8081",
    "servicePath": "/hdf5-viewer/hdf5-web-gui/"
```

Setup the application to use this configuration file:
```bash
make configure-dev-demo
```

![screenshot configure-dev-demo](screenshots/screenshot_configure-dev-demo.png)

Then open a web browser, and point it to the localhost, using whatever port
your web server is running on, as shown in the following example url, which
uses http, port 8080, and the "servicePath" defined in config-dev-demo.json:

[http://localhost:8080/hdf5-viewer/hdf5-web-gui/](http://localhost:8080/hdf5-viewer/hdf5-web-gui/)

![screenshot welcome demo](screenshots/screenshot_welcome_demo.png)

#### RUN DEVELOPMENT MODE - CAS & HTTPS
To run in development mode with CAS & https, it is almost the same as running
the demo mode, the only differences being that the local web server needs
to be setup with proper ssl certificates and be running https, one must
configure the server, and then change the front-end configuration to use it.
Also, set the location of this front-end repository relative to the base
directory of the web server:
```bash
vim config/config-dev-cas-https.json
    "hdf5DataServer": "https://jasbru.maxiv.lu.se:8081",
    "servicePath": "/hdf5-viewer/hdf5-web-gui/"
```

Setup the application to use this configuration file:
```bash
make configure-dev-cas-https
```
Then open a web browser, and point it to the web server url, using whatever
port your web server is running on, as shown in the following example url,
which uses https, port 9443, and the "servicePath" defined in
config-dev-demo.json:

[https://jasbru.maxiv.lu.se:9443/hdf5-viewer/hdf5-web-gui/](https://jasbru.maxiv.lu.se:9443/hdf5-viewer/hdf5-web-gui/)


#### JAVASCRIPT DEBUGGING
One reason to run the application in development mode is the ability to debug
the javascript files:
```bash
js/
├── ajax-spinner.js
├── cas-handle-login.js
├── cas-handle-ticket.js
├── data-display.js
├── file-navigation.js
├── handle-dataset.js
├── mobile-check.js
├── nav-menu.js
├── page-load.js
├── server-communication.js
└── theme-toggle.js
```

Most javascript functions in these files have debug flags which are by
default set to false, but can be set to true and then their output can be
seen in a web browser debugging window.

To turn on debugging, search for lines like the following in the javascripts
files and change "false" to "true":
```bash
var debug = false;
```

Don't forget though to turn these all back to false before rebuilding the
docker images!!

![screenshot js debugging](screenshots/screenshot_js_debugging.png)


## DEMO SERVER
There is a live demo server running at MAX IV here:
[demo.maxiv.lu.se](http://demo.maxiv.lu.se/)

The front-end is setup to run on port 80, and the back-end on port 443.
These two ports were used as many institutions often block access to most other
ports on outside servers.

### DEMO SERVER SETUP
For MAX IV staff needing to setup the demo instance of the HDF5 Viewer running
on the demo server, do the following, assuming nothing has been installed yet
and that you have sudo access:

ssh to the demo machine:
```bash
ssh demo.maxiv.lu.se
```

#### DEMO SERVER SETUP - SSL CERTIFICATES
Check the age of the certificates (sorta).  If they are more than two years
old, then you probably need to get new ones:
```bash
ls -l /etc/ssl/certs/demo_maxiv_lu_se.*
```

#### DEMO SERVER SETUP - GET CODE
Make directories, get code:
```bash
cd /var/www/
sudo mkdir hdf5-viewer
cd hdf5-viewer/

git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-simple-server.git
git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-web-gui.git

sudo chown $USER:$GROUPS hdf5-*
```

#### DEMO SERVER SETUP - BUILD IMAGES
Build docker images:
```bash
cd /var/www/hdf5-simple-server/
make build

cd /var/www/hdf5-web-gui/
make build
```

Fix an issue with cgroups and docker (if it's already fixed, nothing will
happen when the following is run):
```bash
cd /var/www/hdf5-web-gui/
make cgroup-error-fix
```

#### DEMO SERVER SETUP - CONFIGURE
Now configure things - this will be a bit of a mullet setup (business up front,
party in the back) - where an insecure port (80) is use for the front-end, and
a secure port (443) is used in the back-end.

Configure server part, altering just the listed lines here, so that https is
used, the location of the certificates is set, and the local demouser will
be the user accessessing the data:
```bash
cd /var/www/hdf5-simple-server/

vim config/config-demo.cfg
    HOST_NAME='demo.maxiv.lu.se'
    PROTOCOL='https'
    DEMO_USER='demouser'

vim docker/docker-compose-demo.yml
    - /etc/ssl/certs/demo_maxiv_lu_se.crt:/app/certs/ssl_cert.crt:ro
    - /etc/ssl/certs/demo_maxiv_lu_se.key:/app/certs/ssl_cert.key:ro

    - ../user-info/passwd:/etc/passwd:ro
    - ../user-info/group:/etc/group:ro

    - "443:8081"
```

Configure the front-end to read from the server on port 443, and output things
on port 80:
```bash
cd /var/www/hdf5-viewer/hdf5-web-gui/

vim config/config-demo.json
    "hdf5DataServer": "https://demo.maxiv.lu.se:443",

vim docker/docker-compose-demo.yml
    - "80:8080"
```

#### DEMO SERVER SETUP - START
Start up the server:
```bash
cd /var/www/hdf5-simple-server/
make update-user-info
make start-demo
make logs
```

Start up the front-end:
```bash
cd /var/www/hdf5-viewer/hdf5-web-gui/
make start-demo
make logs
```

#### DEMO SERVER SETUP - CHECK OUTPUT
Open the viewer in a web browser:
[demo.maxiv.lu.se](http://demo.maxiv.lu.se/)
