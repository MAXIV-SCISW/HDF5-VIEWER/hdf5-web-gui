/*global $*/
'use strict';

// External libraries
var FILE_NAV, CAS_TICKET, AJAX_SPINNER, DATA_DISPLAY, THEME_TOGGLE,
    CONFIG_DATA, HANDLE_DATASET,

    // The gloabl variables for this applicaiton
    PAGE_LOAD = {

        "useDarkTheme" : true,
        "mobileView" : undefined,

        // This function is to be called when the page is loaded
        //  - assumes the url has already been checked for a CAS ticket
        //  - load the data tree or display a message
        //  - display a welcome message
        //  - show hidden items
        initialPageLoad : function (dataPathExists, dataPath) {

            var debug = false;

            if (debug) {
                console.debug('*** START js/page-load.js initialPageLoad ***');
                console.log('  Getting root directory contents...');
                console.log('  dataPathExists: ' + dataPathExists);
                console.log('  dataPath:       ' + dataPath);
            }

            // Get root data directory contents
            $.when(HANDLE_DATASET.requestForData('/', false, false,
                    false)).then(
                function (returnValue) {

                    if (debug) {
                        console.log('  js/page-load.js loaded / : ' +
                            returnValue);
                    }

                    // Hide simple loading status messages
                    $('#loadMsg').hide();

                    // Display welcome message, unless immediately loading data
                    if (!dataPathExists) {
                        PAGE_LOAD.displayWelcomeMessage();
                    }

                    // Best to wait until DOM items are in place before doing a
                    // few things
                    $(document).ready(function () {
                        PAGE_LOAD.toggleLoginItems();

                        // If this was a data request from the url (coming from
                        // a page load), then get the data and open the file
                        // tree up to this point.
                        if (dataPathExists) {
                            HANDLE_DATASET.getTreePath(dataPath, true);
                        }
                    });

                    if (debug) {
                        console.debug('***  END  js/page-load.js ' +
                            'initialPageLoad ***');
                    }

                }
            );

        },


        // Say hello!
        displayWelcomeMessage : function () {

            var message;

            // Stop the loader
            AJAX_SPINNER.doneLoadingData();

            // Create the welcome message
            message = '<p>Welcome ' + CAS_TICKET.firstName + '!</p>' +
                      '<p>(click stuff on the left)</p>';

            // Un-hide the welcome message
            document.getElementById("welcomeContent").innerHTML = message;
            $("#welcomeDiv").removeClass('hidden');
        },


        // Depending on login status, show the login or logout buttons and
        // some other items
        toggleLoginItems : function () {

            var i, alwaysShow = ['#navbar'],
                whenInShow = ['#side-nav-menu', '#displayContainer'],
                whenLoggedInShow = ['#logout-btn', '#logout-btn-mobile'];

            // Mobile display?
            PAGE_LOAD.mobileView = window.mobilecheck();

            // Some thigs are initially hidden, as they look ugly without the
            // proper js and css loaded, but they should eventully be shown
            for (i = 0; i < alwaysShow.length; i += 1) {
                $(alwaysShow[i]).toggleClass('hidden');
            }

            // Show or hide the login & logout related items
            for (i = 0; i < whenInShow.length; i += 1) {
                $(whenInShow[i]).toggleClass('hidden');
            }
            // Show or hide the login & logout related items
            for (i = 0; i < whenLoggedInShow.length; i += 1) {
                $(whenLoggedInShow[i]).toggleClass('hidden');
            }

        },


        // Load javascript files on-demand
        loadJavaScriptScripts : function (group) {

            var debug = false, promises = [], scripts = [],
                version = '?v=201811201349';

            if (group === 1) {
                scripts = [
                    '../js/data-display.js',
                    '../js/handle-dataset.js',
                    '../js/theme-toggle.js',
                    '../js/nav-menu.js',
                ];
            }

            if (group === 2) {
                scripts = ['../lib/js/plotly/1.41.2/plotly-latest.min.js'];
            }

            scripts.forEach(function (script) {
                promises.push($.getScript(script + version));
            });

            return $.when.apply(null, promises).done(function () {
                if (debug) {
                    console.log('All done loading javascript!');
                }

                // Allow the loader to be shown again after having hidden it
                // during the background loading of plotly
                if (group === 2) {
                    AJAX_SPINNER.hideLoader = false;
                }

                return true;
            });

        },


        // Load a css file by appending it the html header
        getCSS : function (cssFileUrl) {
            $('<link>').appendTo('head').attr({
                type: 'text/css',
                rel: 'stylesheet',
                href: cssFileUrl,
            });
        },


        // Load a bunch of css files
        loadCSSFiles : function (group) {

            var cssFiles, version = '?v=201811201349';

            if (group === 0) {
                cssFiles = [
                    '../lib/css/jstree/3.2.1/themes/default/style.min.css',
                    '../lib/css/jstree/3.2.1/themes/default-dark/' +
                        'style.min.css',
                    '../css/index.css',
                    '../css/plot-controls.css',
                    '../css/nav-menu.css',
                    '../css/nav-bar.css',
                    '../lib/css/font-awesome/4.7.0/css/font-awesome.min.css',
                ];
            }

            if (group === 1) {
                cssFiles = [
                    '../css/theme-toggle.css',
                ];
            }

            cssFiles.forEach(function (file) {
                PAGE_LOAD.getCSS(file + version);
            });

        },

    };
