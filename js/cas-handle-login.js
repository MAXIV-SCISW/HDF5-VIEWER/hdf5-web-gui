/*global $, atob*/
'use strict';

// External libraries
var SERVER_COMMUNICATION, Ping, CONFIG_DATA, CAS_TICKET, PAGE_LOAD,

    // The gloabl variables for this applicaiton
    CAS_HANDLE_LOGIN = {

        jwtServer : '',
        casServer : '',
        runDemo : false,

        checkJWTCookie : function () {

            var debug = false, cookieName, cookieValue, jwtTokenDetails,
                authUrl, serviceUrl;

            if (debug) {
                console.debug('*** START js/cas-handle-login.js  ' +
                    'checkJWTCookie ***');
            }

            // Check for the MAXIV JWT cookie
            cookieName = "maxiv-jwt-auth";
            cookieValue = CAS_HANDLE_LOGIN.readCookie(cookieName);
            if (debug) {
                console.log("cookieName: " + cookieName);
                console.log("cookieValue:" + cookieValue);
            }

            // Parse the cookie, if it exists
            if (cookieValue) {
                if (debug) {
                    jwtTokenDetails =
                        CAS_HANDLE_LOGIN.parseJwt(cookieValue);
                    console.log("jwtTokenDetails:");
                    console.log(jwtTokenDetails);
                }

            // Redirect to the JWT login page if there is no cookie
            } else {
                if (debug) {
                    console.log("No cookie, need to login");
                }

                authUrl = "https://auth.maxiv.lu.se/login/?redirect=";
                serviceUrl = window.location.origin + CONFIG_DATA.servicePath;
                console.log("redirecting to: " + authUrl + serviceUrl);

                window.location.replace(authUrl + serviceUrl);
                return null;
            }

            return cookieValue;
        },


        whatToDoFirstJWT: function () {

            // This function is a modification of whatToDoFirst(), but now
            // jwt is used instead of cas.
            //
            //

            var debug = false, cookieValue, queryParams = {},
                dataPathExists = false, dataPath = '';

            if (debug) {
                console.debug('*** START js/cas-handle-login.js  ' +
                    'whatToDoFirstJWT ***');
            }

            // Set some loading information to the web page
            document.getElementById("loadMsg").innerHTML =
                'Checking session status, query parameters... ';

            // Check the url for parameters
            queryParams = CAS_HANDLE_LOGIN.checkUrlForParameters();
            dataPathExists = queryParams.hasOwnProperty('data');
            if (dataPathExists) {
                dataPath = queryParams.data;
                if (dataPath === '' || dataPath === '/') {
                    dataPathExists = false;
                }
            }

            if (debug) {
                console.log('dataPathExists: ' + dataPathExists);
                console.log('dataPath:       ' + dataPath);
            }

            // Check for cookie, redirect to login if not present
            cookieValue = CAS_HANDLE_LOGIN.checkJWTCookie();

            if (debug) {
                console.log(cookieValue);
            }

            CAS_TICKET.handleTicket(cookieValue, dataPathExists, dataPath);

            if (debug) {
                console.debug('***  END  js/cas-handle-login' +
                    '.js whatToDoFirstJWT ***');
            }
        },

        whatToDoFirst: function () {

            // This function checks for a few things:
            //   1 - if there is a server session already
            //   2 - for a ticket in the url
            //   3 - for a data path in the url
            //
            // And then does the following:
            //   1 - if ticket, verify and then load page
            //      * If there is a data path, send that along too
            //   2 - if no ticket and no session - send to CAS login page
            //      * If there is a data path, send that along too
            //   3 - if session
            //      * - if no data path, load page
            //      * - if data path, load page, display data, open menu tree

            var debug = false, htmlLoadMsg = '', queryParams = {},
                ticketExists = false, dataPathExists = false, dataPath = '',
                ticket = '';

            if (debug) {
                console.debug('*** START js/cas-handle-login.js  ' +
                            'whatToDoFirst ***');
            }

            // Set some loading information to the web page
            document.getElementById("loadMsg").innerHTML =
                'Checking session status, query parameters... ';

            // Check the url for parameters
            queryParams = CAS_HANDLE_LOGIN.checkUrlForParameters();

            ticketExists = queryParams.hasOwnProperty('ticket');
            dataPathExists = queryParams.hasOwnProperty('data');

            if (dataPathExists) {
                dataPath = queryParams.data;
                if (dataPath === '' || dataPath === '/') {
                    dataPathExists = false;
                }
            }

            if (ticketExists) {
                ticket = queryParams.ticket;
            }

            if (debug) {
                console.log('ticketExists:   ' + ticketExists);
                console.log('dataPathExists: ' + dataPathExists);
                console.log('dataPath:       ' + dataPath);
                console.log('ticket:         ' + ticket);
            }

            // Checking with the server if the user is logged in takes a little
            // time, need to wait for the response
            $.when(CAS_HANDLE_LOGIN.getSessionStatus()).then(
                function (sessionStatusResponse) {

                    if (debug) {
                        console.log('Session status results:');
                        console.log(sessionStatusResponse);
                    }

                    // If there is a ticket in the url, verify it and load the
                    // page
                    if (ticketExists) {
                        if (debug) {
                            console.log('Ticket found, handle it!');
                            console.debug('***  END  ' +
                                        'js/cas-handle-login.js  ' +
                                        'whatToDoFirst ***');
                        }

                        CAS_TICKET.handleTicket(ticket, dataPathExists,
                            dataPath);
                    }

                    // If there is no ticket and no session, redirect to CAS
                    if (!ticketExists &&
                            !sessionStatusResponse.sessionStatus) {

                        htmlLoadMsg += '<span style="color:blue">no</span>';

                        if (debug) {
                            console.log('No session exists, redirect to CAS');
                        }

                        document.getElementById("loadMsg").innerHTML +=
                            htmlLoadMsg;

                        if (debug) {
                            console.debug('***  END  js' +
                                '/cas-handle-login.js  whatToDoFirst ***');
                        }

                        CAS_HANDLE_LOGIN.loginCAS(dataPath);
                    }

                    // If a session exists load the page
                    if (sessionStatusResponse.sessionStatus) {

                        if (debug) {
                            console.log('A session already exists, ' +
                                'will now load the page');
                        }

                        // Save information from the response
                        CAS_TICKET.userName = sessionStatusResponse.userName;
                        CAS_TICKET.firstName = sessionStatusResponse.firstName;

                        // Set some loading information to the web page
                        htmlLoadMsg += '<span style="color:blue">yes</span>  ';
                        htmlLoadMsg += 'User name: <span style="color:blue">' +
                            CAS_TICKET.userName + '</span>  ';


                        if (!dataPathExists) {
                            htmlLoadMsg += 'Loading root data directories...';

                            document.getElementById("loadMsg").innerHTML +=
                                htmlLoadMsg;

                        } else {
                            dataPath = queryParams.data;

                            htmlLoadMsg += 'Loading root data directories...';
                            htmlLoadMsg += '<br/>Looking for data ' + dataPath;

                            document.getElementById("loadMsg").innerHTML +=
                                htmlLoadMsg;
                        }

                        if (debug) {
                            console.debug('***  END  js/cas-handle-login' +
                                '.js whatToDoFirst ***');
                        }

                        // Load the rest of the page
                        PAGE_LOAD.initialPageLoad(dataPathExists, dataPath);
                    }
                }
            );
        },


        // Check the current url for ticket information and save it, then
        // remove that information from the url
        checkUrlForParameters : function () {

            var debug = false, url, queryString, queryParams = {}, param,
                params, i;

            if (debug) {
                console.log('*** START js/cas-handle-login.js  ' +
                    'checkUrlForParameters ***');
            }

            // Get the full url
            url = window.location.href;

            if (debug) {
                console.log('url: ' + url);
                console.log('Checking for url parameters');
            }

            // Get the ticket information
            queryString = window.location.search.substring(1);

            if (debug) {
                console.log('queryString: ' + queryString);
            }

            // Look for any parameters, save them
            params = queryString.split("&");
            for (i = 0; i < params.length; i += 1) {
                param = params[i].split('=');
                queryParams[param[0]] = param[1];
            }

            if (debug) {
                console.log('Parameters found:');
                console.log(queryParams);

                console.log('*** END   js/cas-handle-login.js  ' +
                    'checkUrlForParameters ***');
            }

            return queryParams;
        },


        // Log into the CAS server
        loginCAS : function (dataPath) {

            var debug = false, redirectUrl, htmlLoadMsg = '', serviceUrl = '';

            if (debug) {
                console.debug('*** START js/cas-handle-login.js loginCAS ***');
                console.log('dataPath: ' + dataPath);
            }

            document.getElementById("loadMsg").innerHTML =
                'Attempting to contact HDF5 server... ';

            // From the h5 server, get the CAS server configuration
            $.when(CAS_HANDLE_LOGIN.getCASConfig()).then(function (response) {

                if (debug) {
                    console.log('HDF5 server reached ');
                    console.log(response);
                }

                htmlLoadMsg += '<span style="color:green">success</span></br>';
                htmlLoadMsg += 'Checking if authentication is required... ';

                CAS_HANDLE_LOGIN.casServer = response.casServer;
                CAS_HANDLE_LOGIN.runDemo = (response.runDemo === "true");

                if (debug) {
                    console.log('cas server:  ' + CAS_HANDLE_LOGIN.casServer);
                    console.log('run demo:    ' + CAS_HANDLE_LOGIN.runDemo);
                }

                serviceUrl = window.location.origin + CONFIG_DATA.servicePath;
                if (dataPath !== '') {
                    serviceUrl += '?data=' + dataPath;
                }

                if (debug) {
                    console.log('serviceUrl: ' + serviceUrl);
                }

                // Check if the demo version is being used and if the CAS url
                // given is valid
                if (!CAS_HANDLE_LOGIN.runDemo &&
                        CAS_HANDLE_LOGIN.isValidURL(
                            CAS_HANDLE_LOGIN.casServer
                        )) {

                    htmlLoadMsg += '<span style="color:blue">yes</span>';
                    htmlLoadMsg += '</br>Attempting to contact ';
                    htmlLoadMsg += 'authentication server... ';

                    // Construct the login url which contains the service url
                    // to which the browser will be redirected after
                    // successfully logging into CAS
                    redirectUrl = CAS_HANDLE_LOGIN.casServer +
                        '?redirect=' + encodeURIComponent(serviceUrl);

                    if (debug) {
                        console.debug('Redirecting to ' + redirectUrl);
                    }

                    htmlLoadMsg += 'Redirecting to ' + redirectUrl;

                    if (debug) {
                        console.log('Redirecting to ' + redirectUrl);
                    }

                    // Place some info on html page
                    document.getElementById("loadMsg").innerHTML +=
                        htmlLoadMsg;

                    // Redirect to either the CAS server or directly to the app
                    // window.location = redirectUrl;
                    window.location.replace(redirectUrl);

                } else {

                    if (debug) {
                        console.log('Invalid CAS url given, assumming no CAS' +
                            ' authentication is required and that we\'re ' +
                            'running in demo mode.');
                    }

                    // Url to redirect to the app page, skipping CAS
                    redirectUrl = serviceUrl;

                    htmlLoadMsg += '<span style="color:blue">no</span>';

                    // Place some info on html page
                    document.getElementById("loadMsg").innerHTML +=
                        htmlLoadMsg;

                    // Load some javascript and make sure to remove any
                    // leftover cookies
                    return $.when(CAS_HANDLE_LOGIN.setupDemo()).then(
                        function (response) {

                            // For multiple functions in $.when(), the
                            // responses are saved into an array - take the
                            // right one!
                            var ticketCheck = response;

                            // Save the login name and login status
                            if (ticketCheck.hasOwnProperty('validated')) {
                                CAS_TICKET.isLoggedIn = ticketCheck.validated;
                            }
                            if (ticketCheck.hasOwnProperty('userName')) {
                                CAS_TICKET.userName = ticketCheck.userName;
                            }
                            if (ticketCheck.hasOwnProperty('firstName')) {
                                CAS_TICKET.firstName = ticketCheck.firstName;
                            }

                            if (CAS_TICKET.firstName === '') {
                                CAS_TICKET.firstName = CAS_TICKET.userName;
                            }

                            // Continue with loading the rest of the page
                            PAGE_LOAD.initialPageLoad(false, false);

                            return true;
                        }
                    );
                }

            });

            if (debug) {
                console.debug('***  END  js/cas-handle-login.js loginCAS ***');
            }
        },


        // Log out of the application and CAS
        logoutCAS : function () {

            var debug = false, redirectUrl;

            redirectUrl = CONFIG_DATA.hdf5DataServer + '/logout';

            if (debug) {
                console.log('Redirecting to ' + redirectUrl);
            }

            window.location = redirectUrl;
        },


        // Log out of the application and CAS
        logoutJWT : function () {

            var debug = false, authUrl, serviceUrl;

            if (debug) {
                console.debug('*** START js/cas-handle-login.js  ' +
                    'logoutJWT ***');
            }

            // Trying to end session via auth.maxiv - no luck
            // $.ajax({
            //     type: 'POST',
            //     url: url,
            //     success: function (response) {
            //         console.log("successfully contaced " + url);
            //         console.log("response: " + response);
            //         console.log("response.headers: " + response.headers);
            //
            //     },
            //     error: function () {
            //         console.log("error");
            //     }
            // });

            // Just delete the cookie
            document.cookie = "maxiv-jwt-auth=; " +
                "Domain=maxiv.lu.se;" +
                "Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT";
            if (debug) {
                console.log("maxiv-jwt-auth: " +
                    CAS_HANDLE_LOGIN.readCookie("maxiv-jwt-auth"));
            }

            // Then redirect to the login page, attaching the application url
            authUrl = "https://auth.maxiv.lu.se/login/?redirect=";
            serviceUrl = window.location.origin + CONFIG_DATA.servicePath;
            if (debug) {
                console.log("redirecting to: " + authUrl + serviceUrl);
            }
            window.location.replace(authUrl + serviceUrl);
        },


        // Check with the HDF5 server to see if CAS use is configured
        getSessionStatus : function () {

            var debug = false, casCheckUrl =
                CONFIG_DATA.hdf5DataServer + '/sessionstatus/';

            if (debug) {
                console.log('casCheckUrl: ' + casCheckUrl);
            }

            return SERVER_COMMUNICATION.ajaxRequest(casCheckUrl, debug);
        },


        // Check with the HDF5 server to see if CAS use is configured
        getCASConfig : function () {

            var debug = false, casCheckUrl =
                CONFIG_DATA.hdf5DataServer + '/usecas/';

            if (debug) {
                console.log('casCheckUrl: ' + casCheckUrl);
            }

            return SERVER_COMMUNICATION.ajaxRequest(casCheckUrl, debug);
        },


        // Check with the HDF5 server to see if CAS use is configured
        getJWTConfig : function () {

            var debug = false, jwtCheckUrl =
                CONFIG_DATA.hdf5DataServer + '/usejwt/';

            if (debug) {
                console.log('jwtCheckUrl: ' + jwtCheckUrl);
            }

            return SERVER_COMMUNICATION.ajaxRequest(jwtCheckUrl, debug);
        },


        // Setup the server to use the demo
        setupDemo: function () {

            var debug = false, ticketCheckUrl =
                CONFIG_DATA.hdf5DataServer + '/setupdemo/';

            if (debug) {
                console.log('ticketCheckUrl: ' + ticketCheckUrl);
            }

            return SERVER_COMMUNICATION.ajaxRequest(ticketCheckUrl, false);
        },


        // Check if a given string is a valid url
        isValidURL : function (str) {

            var debug = false, parser = document.createElement('a');
            parser.href = str;

            if (str === 'None') {
                str = '';
            }

            if (debug) {
                console.log(str);
            }

            return (parser.host && parser.host !== window.location.host);
        },


        readCookie : function (name) {
            var debug = false, nameEQ = name + "=",
                ca = document.cookie.split(';'), i, c;

            if (debug) {
                console.log("*** readCookie(" + name + ") called ***");
                console.log("document.cookie:");
                console.log(document.cookie);
            }

            for (i = 0; i < ca.length; i += 1) {
                c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return null;
        },


        parseJwt : function (token) {
            try {
                return JSON.parse(atob(token.split('.')[1]));
            } catch (e) {
                return null;
            }
        },

    };

$(document).ready(function () {

    CAS_HANDLE_LOGIN.whatToDoFirstJWT();
});
