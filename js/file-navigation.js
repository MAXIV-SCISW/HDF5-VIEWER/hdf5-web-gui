/*global $*/
'use strict';


// External libraries
var SERVER_COMMUNICATION, HANDLE_DATASET, DATA_DISPLAY, CONFIG_DATA, NAV_MENU,
    EventSource,

    // Some gloabl variables
    FILE_NAV =
    {
        jstreeArray : false,
        processSelectNodeEvent : true,
        idsToReload : [],
        data : null,
        useDarkTheme : true,
        temp : false,
        resize : false,
        hasTimeOut : false,
        nodeContentHeight : 0,
        nodeContentWidth : 0,


        // Error message - empty file, folder, no user access
        displayErrorMessage : function (objectTitle, message) {
            // Display the message
            DATA_DISPLAY.drawText(objectTitle, message, '#ad3a3a');
        },


        // Add new item to the file browser tree
        addToTree : function (itemList, selectedId) {

            var debug = false, keyTitle = '', icon = '', doesNodeExist = false,
                needToRefresh = false, newTree = false, nodeCharCount;

            if (debug) {
                console.debug('*** START js/file-navigation.js addToTree ***');
                console.log('  selectedId: ' + selectedId);
            }

            // If the tree is being created, create the jstree object array
            if (FILE_NAV.jstreeArray === false) {
                FILE_NAV.jstreeArray = [];
                newTree = true;
            }

            if (debug) {
                console.log('  newTree: ' + newTree);
                if (itemList === false) {
                    console.log('folder contents are empty');
                }
            }

            // Save the height of the node's content (24 px per inner node)
            // and reset the width for the coming loop
            FILE_NAV.nodeContentHeight = Object.keys(itemList).length * 24;
            FILE_NAV.nodeContentWidth = 0;

            // Loop over the list of items
            for (keyTitle in itemList) {
                if (itemList.hasOwnProperty(keyTitle)) {

                    if (debug) {
                        console.log(keyTitle + " -> url:             " +
                            itemList[keyTitle].url);
                        console.log(keyTitle + " -> item_type:       " +
                            itemList[keyTitle].item_type);
                        console.log(keyTitle + " -> full_name:       " +
                            itemList[keyTitle].full_name);
                        console.log(keyTitle + " -> short_name:      " +
                            itemList[keyTitle].short_name);
                        console.log(keyTitle + " -> user_can_read:   " +
                            itemList[keyTitle].user_can_read);
                    }

                    // Choose an icon for the tree for this item
                    if (itemList[keyTitle].item_type) {

                        switch (itemList[keyTitle].item_type) {
                        case 'folder':
                        case 'h5_folder':
                            icon = 'fa fa-folder';
                            break;
                        case 'h5_file':
                            icon = 'images/hdf5-16px.png';
                            break;
                        case 'image-series':
                            icon = 'fa fa-stack-overflow';
                            break;
                        case 'image':
                            icon = 'fa fa-area-chart';
                            break;
                        case 'line':
                            icon = 'fa fa-line-chart';
                            break;
                        case 'number':
                            icon = 'fa fa-slack';
                            break;
                        case 'text':
                            icon = 'fa fa-comment';
                            break;
                        default:
                            icon = 'fa fa-question-circle';
                        }

                    } else {
                        icon = 'fa fa-minus-circle';
                    }

                    // Check if this id exists already in the tree
                    doesNodeExist = false;
                    if (!newTree) {
                        doesNodeExist = $('#jstree_div').jstree(true).get_node(
                            itemList[keyTitle].full_name
                        );

                        if (debug) {
                            console.log('doesNodeExist: ');
                            console.log(doesNodeExist);
                        }
                    }

                    // If this has not already been added to the tree and it is
                    // user_can_read by the user, add it to the tree
                    if (!doesNodeExist && itemList[keyTitle].user_can_read) {

                        FILE_NAV.jstreeArray.push({

                            // The specific key-value pairs needed by jstree
                            "id" : itemList[keyTitle].full_name,
                            "parent" : (newTree ? '#' : selectedId),
                            "text" : itemList[keyTitle].short_name,
                            "icon" : icon,

                            // Save some additional information - is this a
                            // good place to put it?
                            "data" : {
                                "item_type" : itemList[keyTitle].item_type,
                                "url" : itemList[keyTitle].url,
                                "full_name" : itemList[keyTitle].full_name,
                                "short_name" : itemList[keyTitle].short_name,
                                "user_can_read" :
                                    itemList[keyTitle].user_can_read,
                            },

                            state : {
                                checkbox_disabled : true
                            },
                        });

                        needToRefresh = true;
                    }

                    // Calculate the longest node name and save it
                    nodeCharCount = itemList[keyTitle].short_name.length;
                    if (nodeCharCount > FILE_NAV.nodeContentWidth) {
                        // Each char is ~6.5px + 24 px for icon
                        FILE_NAV.nodeContentWidth =
                            nodeCharCount * 6.5 + 3 * 24;
                    }
                }
            }

            // Create jstree object
            if (newTree) {

                $('#jstree_div').jstree(
                    {
                        "core" : {
                            "data" : FILE_NAV.jstreeArray,
                            "themes" : {
                                "name" : (FILE_NAV.useDarkTheme === true ?
                                        "default-dark" : "default"),
                                "dots" : true,
                                "icons" : true,

                                // "responsive" : true,
                                "ellipsis" : true,
                            },

                            "expand_selected_onload" : true,
                        },

                        "plugins": ["sort"],
                    }
                );

            // Add to jstree object
            } else {

                if (debug) {
                    console.log('needToRefresh: ' + needToRefresh);
                }

                if (needToRefresh) {
                    FILE_NAV.processSelectNodeEvent = false;
                    $('#jstree_div').jstree(true).settings.core.data =
                        FILE_NAV.jstreeArray;
                    if (!newTree) {
                        $('#jstree_div').jstree(true).refresh(true);
                    }
                } else if (!newTree) {
                    FILE_NAV.keepNodeInView(selectedId);
                }

            }

            if (debug) {
                console.table(FILE_NAV.jstreeArray, ["text", "id"]);
                console.debug('***  END  js/file-navigation.js addToTree ***');
            }
        },


        // Follow the select node so its content is always on visible
        keepNodeInView : function (nodeId) {
            var debug = false, node, sideNav, leftScroll, topScroll;

            if (nodeId !== undefined) {
                // Save node and the side nav as variables
                node = document.getElementById(nodeId);
                sideNav = $('.side-nav-content');

                // Current scroll value
                leftScroll = sideNav.scrollLeft();
                topScroll = sideNav.scrollTop();

                // If expaned content is less than 50 px from the right side
                // or node is outside the screen on the left
                if (node.offsetLeft + FILE_NAV.nodeContentWidth >
                        sideNav[0].clientWidth + sideNav.scrollLeft() - 50
                        || node.offsetLeft < sideNav.scrollLeft()) {

                    // Scroll to the current node's offset
                    leftScroll = node.offsetLeft;
                }

                // If expaned content is less than 50 px from bottom
                if (node.offsetTop + FILE_NAV.nodeContentHeight >
                        sideNav[0].clientHeight + sideNav.scrollTop() - 50) {

                    // If the expanded content is larger than the screen
                    if (FILE_NAV.nodeContentHeight >=
                            sideNav[0].clientHeight) {

                        // Scroll to selected node - 5 px of padding
                        topScroll = node.offsetTop - 5;
                    } else {

                        // Center content
                        topScroll = node.offsetTop - (sideNav[0].clientHeight
                            - FILE_NAV.nodeContentHeight) / 2;
                    }
                }

                if (debug) {
                    console.log('------------TOP----------------');
                    console.log('offset top', node.offsetTop);
                    console.log('content height', FILE_NAV.nodeContentHeight);
                    console.log('if: ' + node.offsetTop +
                        FILE_NAV.nodeContentHeight + ' > ' +
                        sideNav[0].clientHeight + sideNav.scrollTop() - 50);
                    console.log('height', sideNav[0].clientHeight);
                    console.log('scroll top', sideNav.scrollTop());
                    console.log('------------LEFT---------------');
                    console.log('offset left', node.offsetLeft);
                    console.log('content width', FILE_NAV.nodeContentWidth);
                    console.log('if: ' + node.offsetLeft +
                        FILE_NAV.nodeContentWidth + ' > ' +
                        sideNav[0].clientWidth + sideNav.scrollLeft() - 50);
                    console.log('width', sideNav[0].clientWidth);
                    console.log('scroll left', sideNav.scrollLeft());
                    console.log('--------------------------------');
                }

                // Jquery animation of the scroll
                sideNav.animate({
                    scrollTop: topScroll,
                    scrollLeft: leftScroll
                }, 300);
            }
        },


        changeFolderIcon : function (eventInfo, data, openFolder) {

            var debug = false;

            if (debug) {
                console.log(eventInfo);
                console.log(data);
                console.log(openFolder);
            }

            if (data.node.data.item_type === 'folder') {
                if (openFolder) {
                    data.instance.set_icon(data.node, 'fa fa-folder-open');
                } else {
                    data.instance.set_icon(data.node, 'fa fa-folder');
                }
            }

        },


        eventFire : function (el, etype) {
            if (el.fireEvent) {
                el.fireEvent('on' + etype);
            } else {
                var evObj = document.createEvent('Events');
                evObj.initEvent(etype, true, false);
                el.dispatchEvent(evObj);
            }
        },


        // When an item in the tree is clicked, do something - open a folder,
        // file, display an image, etc.
        treeItemClicked : function (eventInfo, data) {

            var debug = false;

            if (debug) {
                console.log('FILE_NAV.processSelectNodeEvent: ' +
                    FILE_NAV.processSelectNodeEvent);
            }

            // Open or close the node graphically
            $('#jstree_div').jstree(true).toggle_node(data.node.id);

            if (debug) {
                console.log('');
                console.log('  tree node event:');
                console.log(eventInfo);
                console.log('');
                console.log('  tree node data:');
                console.log(data);
            }

            if (FILE_NAV.processSelectNodeEvent) {
                if (debug) {
                    console.log(data);
                    console.log(data.selected);
                }

                HANDLE_DATASET.requestForData(data.node.data.full_name, false,
                    true, false);
            } else {
                if (debug) {
                    console.log('tree item selected, didn\'t do shit though');
                }

                // Reset some global variables
                FILE_NAV.processSelectNodeEvent = true;
            }

            if (debug) {
                console.log('FILE_NAV.processSelectNodeEvent: ' +
                    FILE_NAV.processSelectNodeEvent);
            }
        },

    };


// Change the icon when a folder is opened
$("#jstree_div").on('open_node.jstree', function (eventInfo, data) {

    FILE_NAV.changeFolderIcon(eventInfo, data, true);

// Change the icon when a folder is closed
}).on('close_node.jstree', function (eventInfo, data) {

    FILE_NAV.changeFolderIcon(eventInfo, data, false);
});


// When an item in the tree is clicked, do some stuff
$('#jstree_div').on("select_node.jstree", function (eventInfo, data) {
    var debug = false;

    if (debug) {
        console.log('*** START file-navigation.js on("select_node.jstree")');
        console.log('  eventInfo:');
        console.log(eventInfo);
        console.log('  data:');
        console.log(data);
    }

    FILE_NAV.treeItemClicked(eventInfo, data);
});


// Follow recently expanded node
$('#jstree_div').on('refresh.jstree', function (e, data) {
    var debug = false, nodeId = $('#jstree_div').jstree('get_selected')[0];

    if (debug) {
        console.log(e);
        console.log(data);
    }

    // Check if there is a selected node, otherwise give some
    // time for the tree to propagate. This triggers a lot on
    // start up so we need a bool to see if we've been here b4
    if (!FILE_NAV.hasTimeOut) {
        if (nodeId === undefined) {
            setTimeout(function () {
                nodeId = $('#jstree_div').jstree('get_selected')[0];
                FILE_NAV.keepNodeInView(nodeId);
                FILE_NAV.hasTimeOut = false;
            }, 500);

            FILE_NAV.hasTimeOut = true;

        } else {
            FILE_NAV.keepNodeInView(nodeId);
        }
    }

});

// Enable resizing when mousedown on the resize bar
$('#resizeable-bar').on('mousedown', function (e) {
    FILE_NAV.resize = true;
    e.preventDefault();
});

// If user has been resizing redraw the plot on mouseup
// and disable resizing
$(window).on('mouseup', function () {
    if (FILE_NAV.resize) {
        FILE_NAV.resize = false;
        DATA_DISPLAY.redrawPlotCanvas(100);
    }
});

// If user is resizing change width of the side nav menu
$(window).on('mousemove', function (e) {
    var mouseX = 0;

    if (FILE_NAV.resize) {
        mouseX = e.originalEvent.clientX;
        $('#side-nav-menu').width(mouseX);
    }
});
