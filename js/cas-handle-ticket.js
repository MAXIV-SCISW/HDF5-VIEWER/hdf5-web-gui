/*global $, atob*/
'use strict';

// External libraries
var SERVER_COMMUNICATION, PAGE_LOAD, CAS_LOGIN_LOGOUT, CONFIG_DATA,

    // The gloabl variables for this applicaiton
    CAS_TICKET = {

        firstName : '',
        userName : '',
        loginNeeded : true,
        isLoggedIn : false,


        // Send a CAS server ticket to the HDF5 server, which will then
        // verify the ticket and create a cookie containing CAS information
        validateTicket: function (casTicket, dataPath) {

            var debug = false, serviceUrl = '', ticketCheckUrl = '';

            serviceUrl = window.location.origin + CONFIG_DATA.servicePath;
            if (dataPath !== '') {
                serviceUrl += '?data=' + dataPath;
            }

            ticketCheckUrl = CONFIG_DATA.hdf5DataServer +
                '/validate?ticket=' + casTicket +
                '&service=' + serviceUrl;

            if (debug) {
                console.log('serviceUrl    : ' + serviceUrl);
                console.log('ticketCheckUrl: ' + ticketCheckUrl);
            }

            return SERVER_COMMUNICATION.ajaxRequest(ticketCheckUrl, false);
        },


        parseJwt : function (token) {
            try {
                return JSON.parse(atob(token.split('.')[1]));
            } catch (e) {
                return null;
            }
        },

        validateJWT: function (casTicket, dataPath) {

            var debug = false, serviceUrl = '', ticketCheckUrl = '',
                tokenValue;

            if (debug) {
                console.debug('*** START js/cas-handle-ticket.js  ' +
                    'validateJWT ***');
                console.log(casTicket);
            }

            tokenValue = CAS_TICKET.parseJwt(casTicket);
            if (debug) {
                console.log(tokenValue);
            }

            serviceUrl = window.location.origin + CONFIG_DATA.servicePath;
            if (dataPath !== '') {
                serviceUrl += '?data=' + dataPath;
            }

            ticketCheckUrl = CONFIG_DATA.hdf5DataServer +
                '/validatejwt?tokenvalue=' + JSON.stringify(tokenValue) +
                '&ticket=' + casTicket +
                '&service=' + serviceUrl;

            if (debug) {
                console.log('serviceUrl    : ' + serviceUrl);
                console.log('ticketCheckUrl: ' + ticketCheckUrl);
            }

            return SERVER_COMMUNICATION.ajaxRequest(ticketCheckUrl, false);
        },


        // Take a ticket from the CAS server, validate it, then load the page
        handleTicket : function (casTicket, dataPathExists, dataPath) {

            var debug = false;

            if (debug) {
                console.debug('*** START js/cas-handle-ticket.js  ' +
                    'handleTicket ***');
                console.log(casTicket);
            }

            // Clean the url - get rid of eveything after the last /
            // window.history.pushState({}, document.title,
            //     CONFIG_DATA.servicePath);

            CAS_TICKET.loginNeeded = true;

            // Sending the ticket to the HDF5 server and having it verified
            // can be slow, so at the same time load some javascript that
            // will be used in the next step.
            return $.when(
                CAS_TICKET.validateJWT(casTicket, dataPath)
            ).then(
                function (response) {

                    // For multiple functions in $.when(), the responses
                    // are saved into an array - take the right one!
                    var ticketCheck = response;

                    if (debug) {
                        console.log(ticketCheck);
                    }

                    // Save the login name and login status
                    if (ticketCheck.hasOwnProperty('validated')) {
                        CAS_TICKET.isLoggedIn = ticketCheck.validated;
                    }
                    if (ticketCheck.hasOwnProperty('userName')) {
                        CAS_TICKET.userName = ticketCheck.userName;
                    }
                    if (ticketCheck.hasOwnProperty('firstName')) {
                        CAS_TICKET.firstName = ticketCheck.firstName;
                    }

                    if (CAS_TICKET.firstName === '') {
                        CAS_TICKET.firstName = CAS_TICKET.userName;
                    }

                    if (debug) {
                        console.log('CAS_TICKET.isLoggedIn:  ' +
                            CAS_TICKET.isLoggedIn);
                        console.log('User name:  ' + CAS_TICKET.userName);
                        console.log('First name: ' + CAS_TICKET.firstName);
                    }

                    // Continue with loading the rest of the page
                    if (CAS_TICKET.isLoggedIn) {
                        PAGE_LOAD.initialPageLoad(dataPathExists, dataPath);
                    }

                    return CAS_TICKET.isLoggedIn;
                }
            );

        },


        // Load javascript files on-demand
        loadJavaScriptScripts : function (group) {

            var debug = false, promises = [], scripts = [],
                version = '?v=201811201349';

            if (group === 0) {
                scripts = [
                    "../lib/js/jstree/3.2.1/jstree.min.js",
                    "../js/nav-menu.js",
                    "../js/file-navigation.js",
                    "../js/page-load.js",
                ];
            }

            scripts.forEach(function (script) {
                promises.push($.getScript(script + version));
            });

            return $.when.apply(null, promises).done(function () {
                if (debug) {
                    console.log('All done loading javascript!');
                }

                return true;
            });

        },

    };
