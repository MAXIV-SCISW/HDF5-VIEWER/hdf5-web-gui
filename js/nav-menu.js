/*global $*/
'use strict';

// The global variables for this applicaiton
var DATA_DISPLAY, AJAX_SPINNER,

    NAV_MENU = {

        "menuIsOpen" : true,
        "displayContainer" : "displayContainer",
        "displayOffsetWidth" : "0px",

        // If the menu button is pressed
        menuButton : function () {

            var debug = false;

            if (debug) {
                console.log("menuButton() called");
                console.log("NAV_MENU.menuIsOpen:   " + NAV_MENU.menuIsOpen);
            }

            // If the nav menu is open then close it, if it's closed, open it.
            if (NAV_MENU.menuIsOpen) {
                NAV_MENU.closeNav();
            } else {
                NAV_MENU.openNav();
            }

            DATA_DISPLAY.redrawPlotCanvas(100);
        },


        // Open the side menu
        openNav : function () {

            var debug = false;

            // Slide the menu out
            $("#side-nav-menu").removeClass('hidden');

            NAV_MENU.menuIsOpen = true;

            if (debug) {
                console.log('nav open');
            }


        },


        // Close the side menu
        closeNav : function () {

            // Hide the menu
            $("#side-nav-menu").addClass('hidden');

            NAV_MENU.menuIsOpen = false;

        },

    };
